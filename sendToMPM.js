
const toByteArray = (s) => {
    const sry = s.split("");
    const bry = sry.map((c, i) => s.charCodeAt(i));
    return bry;
  };

const toString = (bry) => {
    const theLine = bry.splice(0,bry.indexOf(13))
    const s = String.fromCharCode(...theLine)
    return s;
}

// sendToMPM
// @param mpmDevice - the HID device to send the MPM messages
// @param cmd -- the ASCII CMD string to send
// @param options -- optional config param options
const defaultOptions = {
  waitTime : 3000,    // How long to allow MPM to respond
  rawReturn : false   // Just return whatever you get back
}

 function sendToMPM(mpmDevice, cmd, options) {
  const bytes = toByteArray(cmd);
  const { waitTime,rawReturn } = {...defaultOptions,...options}
  let resp = "";
  let nakd = false;
  let mpmCmd = [...bytes, 13];
  mpmCmd = [0, ...mpmCmd];

  do{
    const nbytes = mpmDevice.sendFeatureReport(mpmCmd);
    try {
      const nb = mpmDevice.readTimeout(waitTime);
      resp = toString(nb);
      nakd = !rawReturn && resp.includes("NAK");
      if(nakd){
        console.log(`...recv'd a NAK, retrying...`)
      }
    } catch (e) {
      resp = `Send Command error: ${e}`;
    }
  }while( nakd );

  return resp;
}

module.exports = sendToMPM;
