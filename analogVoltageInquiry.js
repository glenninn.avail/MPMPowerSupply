const sendToMPM = require("./sendToMPM.js");

// analogVoltageInquiry .js
// Read device's Analog Voltage/Current condition.  Returns:
// DEVICE: (CCC.CCC DDD.DDD EEE.EEE FFF.FFF GGG.GGG HHH.HHH JJJ.JJJ KKK.KKK<cr>
// (	    Start byte
//     CCC.CCC	    AC input voltage (volt)
//     DDD.DDD	    Inverter output voltage (volt)
//     EEE.EEE	    Battery voltage (volt)
//     FFF.FFF	    Output current (amp)
//     GGG.GGG	    Charging current (amp)
//     HHH.HHH	    Discharging current (amp)
//     JJJ.JJJ	    Charger Peak current (amp)
//     KKK.KKK	    Aux DC current (amp)
 

const deviceCommand = "QAV";

function analogVoltageInquiry(device) {
    const info = sendToMPM(device,deviceCommand);

    fields = info.slice(1).split(" ");

    const analogVoltage = { 
        RAW : info,
        acInputVoltage : fields[0],
        inverterOutputVoltage : fields[1],
        batteryVoltage : fields[2],
        outputCurrent : fields[3],
        chargingCurreng : fields[4],
        dischargingCurrent: fields[5],
        chargerPeakCurrent: fields[6],
        auxDCCurrent : fields[7]
    };

    return analogVoltage;
}

module.exports=analogVoltageInquiry;
