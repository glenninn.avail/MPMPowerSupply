const sendToMPM = require("./sendToMPM.js");

// deviceIdInquiry.js
//   Decodes the device information returned from the MPM power supply
// acording to:
// *** Device ID Inquiry ***
// MPM query[ QID ], resp: 
// (02 ## 2 1 ###2020016 0000000000
// (AA YY Q G XXXXXXXXXX YYYYYYYYYY
// Device Type: 02 (MPM-AC)
// Year: ##
// Qtr: 2
// Mfgr ID: 1 (PowerVar)
// Unit serial #: ###2020016
// PCB  serial #: 0000000000

const deviceCommand = "QID";


const deviceType = (field)=> field === "02" ? "MPM-AC" : field;
const manufacturer = (field)=> { switch(field) {
    case "1" : return "POWERVAR";
    case "2" : return "CWC";
    case "3" : return "VP";
    default : field;    
  }
}



 function  deviceIDInquiry(device) {
    const info = sendToMPM(device,deviceCommand);

    fields = info.slice(1).split(" ");

    const deviceId = {
        deviceType: deviceType(fields[0]),
        year: fields[1],
        quarter: fields[2],
        manufacturerId: manufacturer(fields[3]),
        unitSerialNumber: fields[4],
        mainPCBserialNumber: fields[5]
    }

    return deviceId;
}

module.exports=deviceIDInquiry;
