const  HID = require("node-hid");
const readline = require("readline");
const  modelInquiry= require("./modelInquiry.js");
const  generalStatusInquiry = require("./generalStatusParams.js");
const  firmwareVersion = require("./firmwareVersion.js");
const  deviceIDInquiry = require("./deviceIdInquiry.js");
const  currentBatteryInquiry = require("./currentBatteryInquiry.js");
const  batteryDiagnostics0Inquiry = require("./batteryDiagnostics0Inquiry.js");
const  warningStatusInquiry = require("./warningStatusInquiry.js");
const  deviceModeInquiry = require("./deviceModeInquiry.js");
const  analogVoltageInquiry = require("./analogVoltageInquiry.js");
const {restartSupply, clearRestart}  = require("./restartSupply.js");

const jss = (...o) => JSON.stringify(o[0], null, o.length > 1 ? 2 : 0);

const sleep = async (ms)=>{
  const p = new Promise(
    async (finish)=>{
      setTimeout(
        ()=>finish(true),
        ms
      )
    }
  );
  return p;
}


// The USB/HID vendor and product codes for our MPM p/supply
const MPMinfo = {
  vendorId: 9084,
  productId: 4016,
};

function initialize(){ 
  // Configure USB communciations protocol to use with MPM Power Supply
  HID.setDriverType("hidraw");
  const [engine,app,...cliOpts] = process.argv;
  if(cliOpts.length > 0) {
    console.log( `Executing MPM command: "${cliOpts}"`)
  }
  return cliOpts;
}

function dstat(device) {
  console.log(`Device is> ${jss(device, 1)}`);
}


// Wait in a keypress timeout for any async event messages from the
// MPM Power supply.  Useful for testing "pull the plug" to see
// what error messages are returned
async function monitorDevice(device) {
  console.log(`Listening for Async messages from the MPM Power supply`)
  device.on("data", (data)=>{
    console.log(`DATA> ${data}`)
  });

  device.on("error", (err)=>{
    console.log(`ERROR> ${err}`)
  });
  const kp = await waitForKeypress("Monitoring, hit Enter to stop> ")
}



// Show the "prompt" followed by the MPM's report data for the device
function show(prompt, MPMfunction, device) {
  console.log(`**** ${prompt}: ****
${jss( MPMfunction(device),1)}
`)
}



async function waitForKeypress(prompt = "cmd> ") {
  const p = new Promise(  (res)=>{
      const rl = readline.createInterface({
          input: process.stdin,
          output: process.stdout
        });
        
        rl.question(prompt, (answer) => {
          // TODO: Log the answer in a database
          console.log(`You selected: ${answer}`);
          rl.close();
         // clean up command KeyPress(es) to have only 1 space between field(s)
          res(answer.replace(/\s+/g, ' ').trim().split(" "));
        });
  });

  return p;
}


const menu = (commands)=>{
  const nCols = 3;
  let prompt = "";

  const makeEntry = (cmd)=> `${cmd.key} -- ${cmd.desc.padEnd(24,' ')}`;

  const nRows = Math.ceil(commands.length / 3);

  for( let r = 0; r < nRows; r++) {
    for( let c = 0; c < nCols; c++) {
      const index = r*nCols + c;
        prompt += index < commands.length ?  makeEntry( commands[index]) : "";
    }
    prompt += "\n";      
  }
  prompt += `\nCmd?> `;
  return prompt;
}


// Table of CLI command/key, user prompt, and link to the corresponding
// MPMPower Supply Handler to display the corresponding results returned
// from the MPM power supply primitive API
const commands = [
  { key: "m", desc: "Supply's Model Info" , handler: modelInquiry },
  { key: "s", desc: "General Status Info" , handler: generalStatusInquiry },
  { key: "f", desc: "Firmware Version" , handler: firmwareVersion },
  { key: "d", desc: "Device Info" , handler: deviceIDInquiry },
  { key: "D", desc: "Device Mode Info" , handler: deviceModeInquiry },
  { key: "b", desc: "Current Battery Info" , handler: currentBatteryInquiry },
  { key: "w", desc: "Warning Status Info" , handler: warningStatusInquiry },
  { key: "a", desc: "Analog Voltage Info" , handler: analogVoltageInquiry },
  { key: "0", desc: "Battery Diagnostic #0" , handler: batteryDiagnostics0Inquiry },
  { key: "!", desc: "Monitor Device" , handler: monitorDevice },
  { key: "R", desc: "Restart MPM Supply" , handler: null },
  { key: "C", desc: "Clear MPM Restart", handler: clearRestart},
  { key: "q", desc: "quit" , handler: null}
];


function showRestartHelp() {
  console.log(`Restart Power Supply / Cart
Usage:  r <Tdown> <Tup>
where:  
  Tdown (secs) is in range [12 .. 600]
  Tup (secs) is in range [12 .. 599,000]
`);
}


const validSecDown = (n) => (n>=12) && (n<=600);
const validSecToStart = (n)=> (n>=12) && (n<=599000);

//Tdown : a 2-character string [".2" .. "10"] (minutes) to wait until power-down
//Tup   : a 4-character string ["00.2" to "9999] (minutes) after power-down to restart"`)
const Tdown = (sec)=> sec < 60 ? (sec/60).toFixed(1).slice(1) : (sec/60).toFixed(0).padStart(2,"0");
const Tup = (sec)=> sec < 6000 ? (sec/60).toFixed(1).padStart(4,"0") : (sec/60).toFixed(0).padStart(4,"0");

const generateRestartCmd = (secDown,secToStart)=> `S${Tdown(secDown)}` + (secToStart < 12 ? "" : `R${Tup(secToStart)}`);


// Function to bounds check the specified command options for MPM Restart of supply
function processRestartOptions(opts) {

  showRestartHelp();

  let timings = {
    secDown : parseInt( (opts.length > 1 ? opts[1] : "12") ),
    secToStart : parseInt( (opts.length > 2 ? opts[2] : "12") ),
    mpmCommand: ""
 }


  // Check time boundaries for both shutdown and restart
  if( !validSecDown(timings.secDown) || !validSecToStart(timings.secToStart) ) {
    console.log(`?Invalid shutdown delay seconds[ ${timings.secDown} ] or restart delay seconds[ ${timings.secToStart} ]`)
    return null;
  }

  // return the effective MPM command string
  timings.mpmCommand = generateRestartCmd( timings.secDown,timings.secToStart);
  return timings;
}

function sendRestartWithRetries(device,mpmRestartInfo) {
  // We have a valid string, try sending with redundancy
  if(mpmRestartInfo){
    let retryCount = 5;
    while(retryCount>0) {
      console.log(`Sending MPM command: "${mpmRestartInfo.mpmCommand}"`)
      const restartResp = restartSupply(device,mpmRestartInfo.secDown,mpmRestartInfo.secToStart);
      console.log(`Restart Supply Responded: ${jss(restartResp,1)}`)
      retryCount = restartResp.response.includes("NAK") ? retryCount-1 : -1;
    }
  }
}

async function runCommand(MPMinfo,opts) {
   const kp = opts[0];
   let device = null;

   try {
     device = new HID.HID(MPMinfo.vendorId, MPMinfo.productId);
   } catch(e) {
      console.log(`Error, unable to connect to MPM device: ${e}`)
      return;
   }

   switch(kp) {
    case "R" : 
        // Get the approved MPM command string, or "" (abort)
        const mpmRestartInfo = processRestartOptions(opts);
        sendRestartWithRetries(device, mpmRestartInfo);
      break;

    default:
      const MPMop = commands.find( cmd=> cmd.key === kp );
      if(MPMop) {
        show( MPMop.desc, MPMop.handler, device)
      } else {
        console.log(`?Unrecognized command key: "${kp}"`)
      }
      break;
   }

   device.close();

}

async function main(MPMinfo) {
  console.log(`MPM - test utility`)

  let done = false;

  do {
   const opts = await waitForKeypress( menu(commands) );
   const kp = opts[0];
   let device = null;
   let inErr = false;

   console.log(`Opts> ${opts}`)

   try {
     device = new HID.HID(MPMinfo.vendorId, MPMinfo.productId);
   } catch(e) {
      console.log(`Error, unable to connect to MPM device: ${e}`)
      inErr = true;
   }

   if(kp ==="q") {
      done = true;
   }

   // If there's no MPM HID device, we can't do any operations
   if(inErr) continue;

   switch(kp) {
    case "h":
    case "?":
      consolelog( menu(commands) );
      break;
    case "!":
      // This is an interactive mode to watch any streamed status messages
      await monitorDevice(device);
      break;
    case "q":
      done = true;
      break;
    case "R" : 
      if(opts.length < 2) {
        showRestartHelp();
      } else {
        // Get the approved MPM command string, or "" (abort)
        const mpmRestartInfo = processRestartOptions(opts);
        if(mpmRestartInfo ) {
          // Give the user a 2nd chance to validate the command before sending
          console.log(
          `Send the comamnd:  "${mpmRestartInfo.mpmCommand}" ?
          `)
          const resps = await waitForKeypress(" y,n,q > ");
          resps[0] = resps[0].toLowerCase();

          if(resps[0] === "y" ) {
            sendRestartWithRetries(device,mpmRestartInfo);
          }
        }
      }
      break;
    default:
      runCommand(MPMinfo,opts);
   }

   device.close();

  }while( !done );
  console.log(`*** done ***`)
}



const cliToRun = initialize();

if(cliToRun.length>0) {
  runCommand(MPMinfo,cliToRun)
} else {
  main(MPMinfo);
}

