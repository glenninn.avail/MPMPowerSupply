const sendToMPM = require("./sendToMPM.js");

// firmwareVersion.js
// Decodes the firmware running on the power supply according to:
// (VERFW:V052520274
// Version 05.25.20274

const deviceCommand = "QVFW";


 function firmwareVersion(device) {
    const info = sendToMPM(device,deviceCommand).toString();

    const header= "(VERFW:V";

    vers = info.slice(header.length);

    const version = {
        major: vers.slice(0,2),
        minor: vers.slice(2,4),
        build: vers.slice(4)
    }
    return version;
}

module.exports=firmwareVersion;

