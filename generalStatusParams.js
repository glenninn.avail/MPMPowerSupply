const sendToMPM = require("./sendToMPM.js");

// generalStatusParams.js

// *** General Status Parameters Inquiry ***
// MPM query[ QGS ], resp: 
// (119.5 121.5 60.0 001.07 053 ---.- 048.4 0400 60.0
// (MMM.M LLL.L NN.N QQQ.QQ DDD SSS.S TTT.T XXXX FF.F

// Vin = 119.5 vac
// Vout = 121.5 vac
// Outputs
// Freq = 60.0 hz
// Current = 1.07 Amps
// Load = 5.3% ??
// Pri. Battry Volts = ---.- (N/A?)
// Temperature = 48.4 Deg C
// Mode Bitmap = 0400
// Input
// Freq = 60.0 Hz

const deviceCommand = "QGS";


 function generalStatusInquiry(device) {
    const info = sendToMPM(device,deviceCommand);

    fields = info.split(" ").slice(1);
    const generalStatus = {
        inputVoltage : parseFloat( fields[0] ),
        outputVoltage : parseFloat(fields[1]),
        outputFreq : parseFloat(fields[2]),
        outputCurrent: parseFloat(fields[3]),
        outputLoad :parseFloat(fields[4]),
        primaryBatteryVoltage: fields[5],
        temperature : parseFloat(fields[6]),
        modeBitmap : fields[7],
        inputFrequency: parseFloat(fields[8])
    }
    return generalStatus;
}

module.exports=generalStatusInquiry;
