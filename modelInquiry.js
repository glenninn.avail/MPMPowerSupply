const sendToMPM = require("./sendToMPM.js");

// qmd.js
//   Decodes the query string returned from the MPM power supply
// according to this:
// TTTTTTTTTTTTTTT WWWWWW KK RR MMM NNN x DD DD DD F
// ######87023-07R ###250 99 60 120 120 038 0 00 00 00 2
//
// Device model: 87023-07R
// Wattage: 250 Watts
// Output Power Factor: 99% (1.0 PF)
// Nominal Frequency (In & Out): 60 Hz
// Nominal Input Voltage: 120 Vac
// Nominal Output Voltage: 120 Vac
// Output Voltages:  (can't decode, maybe due to old f/w)
// Inverter Frequency Config: 60 Hz (2)

const deviceCommand = "QMD";

const toFrequency = (f)=> { switch(f){
    case "1": return "50";
    case "2": return "60";
    default: return f;
}}


 function modelInquiry(device) {
    const info = sendToMPM(device,deviceCommand);

    fields = info.split(" ");
    const deviceInfo = {
        deviceModel: fields[0],
        wattage: fields[1],
        outputPowerFactor: parseInt(fields[2]),
        nominalFrequency: parseInt(fields[3]),
        nominalInputVoltage: parseInt(fields[4]),
        nominalOutputVoltage: parseInt(fields[5]),
        outputVoltages: [],
        inverterFrequency: toFrequency(fields[-1])
    }
    return deviceInfo;
}

module.exports=modelInquiry;
