const sendToMPM = require("./sendToMPM.js");

// warningStatusInquiry.js

// *** Warning Status Parameters Inquiry ***
// MPM query[ QWS ], resp: 
// (YYYYYYYY

const deviceCommand = "QWS";


 function warningStatusInquiry(device) {
    const info = sendToMPM(device,deviceCommand);

    const warningStatus = {
      bits : info.slice(1)
    }
    return warningStatus;
}

module.exports=warningStatusInquiry;
