const sendToMPM = require("./sendToMPM.js");

// restartsSupply.js
// Sends command(s) to shutdown and restart power supply.  The command syntax is
// For shutdown followed by restart:  SnnRmmmm
// For shutdown only:  Snn
// These API's return one of 2 strings
//    "NAK" | "ACK"

// The shutdown and restart timing strings are defiined as follows
//Tdown : a 2-character string [".2" .. "10"] (minutes) to wait until power-down
//Tup   : a 4-character string ["00.2" to "9999] (minutes) after power-down to restart"`)
const Tdown = (sec)=> sec < 60 ? (sec/60).toFixed(1).slice(1) : (sec/60).toFixed(0).padStart(2,"0");
const Tup = (sec)=> sec < 6000 ? (sec/60).toFixed(1).padStart(4,"0") : (sec/60).toFixed(0).padStart(4,"0");


 function restartSupply(device,secDown=12,secUp=0) {

    const deviceCommand = `S${Tdown(secDown)}`
                            + (secUp < 12 ? "" : `R${Tup(secUp)}`);

    const options = {
        rawReturn : true
    };

    console.log(`Sending to MPM: ${deviceCommand}`)

    const info = sendToMPM(device,deviceCommand,options).toString();

    const restartResponse = {
       command: deviceCommand,
       response: info
    }
    return restartResponse;
}

 function clearRestart(device) {
    const deviceCommand = `CS`;
    const options = {
        rawReturn : true
    };
    const info = sendToMPM(device,deviceCommand,options).toString();

    const clearRestartResponse = {
        command : deviceCommand,
        response: info
    };

    return clearRestartResponse;
}

module.exports={restartSupply, clearRestart};

