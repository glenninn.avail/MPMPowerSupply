const sendToMPM = require("./sendToMPM.js");


// *** Battery Diagnostics #0 Inquiry ***
//   Decodes the battery diagnostics (level 0) information returned from 
// the MPM power supply acording to:
// *** Battery Diagnostics #0 Information Query ***
// MPM query[ BDIAG0 ], resp: 
// 6000 00C0 13322 00000 00003 00100 00101 38080 38080 65535 00000 
// MMMM SSSS TTTTT CCCCC AAAAA RRRRR PPPPP QQQQQ FFFFF EEEEE DDDDD YYYYY

// Battery Mode: 0x6000
// Battery Status: 0x00C0
// Battery Terminal Voltage: 133.22 vdc
// Battery Current: 0 Amps
// Avg Battery Current: 3 Amps
// Relative SOC: 100
// Absolute SOC: 101
// Remaining Capacity: 38080
// Full Charge Capacity: 38080
// Runtime to Empty: 65535
// Average Time to Full: 0
// Cycle Count: missing

const deviceCommand = "BDIAG0";

 function batteryDiagnostics0Inquiry(device) {
    const info = sendToMPM(device,deviceCommand);

    console.log(`RAW> ${info}`)
    return {};

    fields = info.split(" ");

    const batteryDiagnostic0 = {
        batteryMode: fields[0].toString(16),
        batteryStatus: fields[1].toString(16),
        batteryTerminalVoltage: fields[2],
        batteryCurrent: fields[3],
        avgBatteryCurrent: fields[4],
        relativeSOC: fields[5],
        absoluteSOC: fields[6],
        remainingCapacity: fields[7],
        fullChargeCapacity: fields[8],
        runtimeToEmpty: fields[9],
        avgTimeToFull: fields[10],
        cycleCount: "(N/A)"
    }

    return batteryDiagnostic0;
}

module.exports=batteryDiagnostics0Inquiry;

