# MPMPowerSupply

This application provides a simple Command Line Interface (CLI) utility to access an MPM Power Supply unit via a USB connection.

***

## Documentation and Specifications
Included in this repository is the PDF for the MPM Power Supply's USB interface.


***
## Requirements
This project is built using `NodeJS` and various `npm` libraries.  It supports a mix of `Typescript` and/or `Javascript` source files.  In order to work in the project, you must have installed the following 

- NodeJS version `14.17.1`.  The project by default attempts to select Node 14.17.1 using the nvm utility.
- npm version `6.14.13`
- node version manager utility, nvm, version `0.39.1`

It is highly recommended to use Visual Code as your IDE.

***

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.


### 1) Clone or checkout the development branch froim the repo...

You can clone the contents of this repo to you local development machine by
```
% git clone git@gitlab.com:glenninn.avail/MPMPowerSupply.git 
% npm install
```

### 2) Running as a CLI utility

First make sure that you are running this on a Mac that is connected to the MPM Power Supply.  This CLI utilze the HID Protocol to lookg for and connect to the MPM Power Supply unit

```
% node mpm.js
MPM - test utility
m -- Supply's Model Info     s -- General Status Info     f -- Firmware Version
d -- Device Info             D -- Device Mode Info        b -- Current Battery Info
w -- Warning Status Info     a -- Analog Voltage Info     0 -- Battery Diagnostic #0
! -- Monitor Device          R -- Restart MPM Supply      C -- Clear MPM Restart
q -- quit                 

Cmd?> 
```

### 3) Invoking a Script from the CLI

You can also type a single CLI instruction to run one of the utility functions like a script.   Some examples are:

```
% node mpm.js m
% node mpm.js R 15 30
```

The CLI parameters represent the same keystroke commands that you would enter when running the app in the interactive Utility mode.



### 4) Building a Deployable CLI utility package

MPMPowerSupply can also be bundled and deployed as a utility application..  To compile/bundle the application, and create a `dist/` folder follow these steps:


```
% npm run build

```

***

## Contacts
For detailed questions on this application, the principle design work was started by
Mark Cooper mcooper@avail.io, and Glenn Inn ginn@avail.io

****
