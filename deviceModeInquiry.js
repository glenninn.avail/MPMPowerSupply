const sendToMPM = require("./sendToMPM.js");

// deviceModeInquiry.js
// Read device operating modes
// DEVICE: (M<cr>
//

const deviceCommand = "QMOD";

const qmodMappings = [
{ name: "Undefined(P)", charCode: "P",code: 1 },
{ name: "Standby mode (AC present)", charCode: "S",code: 2 },
{ name: "Standby mode (No AC)", charCode: "Y", code:3 },
{ name: "Line mode", charCode: "L", code:4 },
{ name: "Battery mode", charCode: "B", code:5 },
{ name: "Self test mode", charCode: "T", code:6 },
{ name: "Fault mode: AC present", charCode: "F", code:7 },
{ name: "Fault mode: Inverter ON", charCode: "G", code:8 },
{ name: "Low Power Mode (ECO) mode", charCode: "E", code:9 },
{ name: "Fault mode - overcharge", charCode: "C", code:10 },
{ name: "Undefined(D)", charCode: "D", code:12 },
{ name: "Query Error", charCode:"NAK", code: -1}
]


 function deviceModeInquiry(device) {
    const info = sendToMPM(device,deviceCommand);

    fields = info.slice(1);

    const qmod = qmodMappings.find( (m)=>fields===m.charCode );

    const deviceMode = { ...qmod }
    
    return deviceMode;
}

module.exports=deviceModeInquiry;
