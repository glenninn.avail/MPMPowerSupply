const sendToMPM = require("./sendToMPM.js");


// *** Current Battery Information Inquiry ***
//   Decodes the current battery information returned from the MPM power supply
// acording to:
// MPM query[ QBV ], resp: 
// (0177 0000 0484 0483 099 32 4 2 0 
// (RRRR SSSS AAAA FFFF PPP TT C

// Battery Remaining Runtime: 177 minutes
// Time remaining to charge: 0 minutes
// Actual Current Charge: 484 Watt-Hours
// Battery State of Charge: 99%
// Battery Temperature: 32 Deg C
// Current Charger State: 4 (Float)
// Battery Type Used: 2 (Lithium)

const deviceCommand = "QBV";

const batteryTypes = (bt)=>{switch(bt){
    case "0" : return "SLA";
    case "1" : return "Lithium (no-comms)";
    case "2" : return "Lithium (SMBus)";
    case "3" : return "Lithium (SMBus - protect)";
    case "4" : return "Lithium (Modbus)";
    default : return bt;
}};

const chargeStates = (cs)=>{switch(cs){
    case "0" : return "Off";
    case "1" : return "PreCharge";
    case "2" : return "Constant Current";
    case "3" : return "Constant Voltage";
    case "4" : return "Float";
    default: return cs;
}}


 function currentBatteryInquiry(device) {
    const info = sendToMPM(device,deviceCommand);

    fields = info.slice(1).split(" ");

    const currentBattery = {
        bateryRemainingRuntime: fields[0],
        timeRemainingToCharge: fields[1],
        actualCurrentCharge: fields[2],
        batteryStateofCharge: fields[3],
        batteryTemperature: fields[4],
        currentChargerState: chargeStates(fields[5]),
        batteryTypeUsed: batteryTypes( fields[6] )
    }

    return currentBattery;
}

module.exports=currentBatteryInquiry;

